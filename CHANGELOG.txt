== Change Log

=== Initializer (21-02-2008)
* Initializer should be redo
Plan todo:
- start with empty merb
- move model first and check RAKE

=== Initial (16-02-2008)

* Start to transform plugins to gems and packages [S. Busso] 
 	sudo gem install radius bluecoth highline redcloth rubypants
 	TODO install redcloth (textile) bluecloth (markdown)
* Radiant bin: Use of rubigen instead of RailsGenerator [S. Busso]
* Need sudo gem install --source http://merbivore.com merb_activerecord

=== Radiant CMS

See http://dev.radiantcms.org/radiant/browser/trunk/radiant/CHANGELOG