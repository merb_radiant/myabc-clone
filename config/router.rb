puts "Compiling routes.."

Merb::Router.prepare do |r|

  # Admin Routes
  r.namespace :admin do |admin|
  
    admin.match "admin", :controller => 'admin/welcome' do |w|
      w.match('/').to(:action => 'index').name('admin')
      w.match('/welcome').to(:action => 'index').name('welcome')
      w.match('/login').to(:action => 'login').name('login')
      w.match('/logout').to(:action => 'logout').name('logout')
    end

    # Export Routes
    admin.match "admin", :controller => 'admin/export' do |ex|
      ex.match('/export').to(:action => 'yaml').name('export')
      ex.match('/export/yaml').to(:action => 'yaml').name('export_yaml')
    end

    # Page Routes
    admin.match "admin", :controller => 'admin/page' do |pag|
      pag.match('/pages').to(:action => 'index')
      pag.match('/pages/edit/:id').to(:action => 'edit')
      pag.match('/pages/:parent_id/child/new').to(:action => 'new')
      pag.match('/pages/new/homepage').to(:action => 'new', :slug => '/', :breadcrumb => 'Home')
      pag.match('/pages/remove/:id').to(:action => 'remove')
      pag.match('/ui/pages/part/add').to(:action => 'add_part')
      pag.match('/ui/pages/children/:id/:level').to(:action => 'children', :level => '1')
      pag.match('/ui/pages/tag_reference').to(:action => 'tag_reference')
      pag.match('/ui/pages/filter_reference').to(:action => 'filter_reference')
      pag.match('/pages/cache/clear').to(:action => 'clear_cache')
    end

    # Layouts Routes
    admin.match "admin", :controller => 'admin/layout' do |lay|
      lay.match('/layouts').to(:action => 'index')
      lay.match('/layouts/edit/:id').to(:action => 'edit')
      lay.match('/layouts/new').to(:action => 'new')
      lay.match('/layouts/remove/:id').to(:action => 'remove')
    end

    # Snippets Routes
    admin.match "admin", :controller => 'admin/snippet' do |snp|
      snp.match('/snippets').to(:action => 'index')
      snp.match('/snippets/edit/:id').to(:action => 'edit')
      snp.match('/snippets/new').to(:action => 'new')
      snp.match('/snippets/remove/:id').to(:action => 'remove')
    end

    # Users Routes
    admin.match "admin", :controller => 'admin/user' do |usr|
      usr.match('/users').to(:action => 'index')
      usr.match('/users/edit/:id').to(:action => 'edit')
      usr.match('/users/new').to(:action => 'new')
      usr.match('/users/remove/:id').to(:action => 'remove')
      usr.match('/preferences').to(:action => 'preferences')
    end

    # Extension Routes
    admin.match "welcome", :controller => '' do |ext|
      ext.match('extensions').to(:action => 'index')
      ext.match('extensions/update').to(:action => 'update')
    end

  end

  # Site URLs
  r.match "welcome", :controller => 'site' do |sit|
    sit.match('').to(:action => 'show_page', :url = '/').name('homepage')
    sit.match('error/404').to(:action => 'not_found').name('not_found')
    sit.match('error/500').to(:action => 'error').name('error')
    
    # Everything else
    sit.match('*url', :action => 'show_page')
  end
  
  # RESTful routes
  # r.resources :posts
  
  # This is the default route for /:controller/:action/:id
  # This is fine for most cases.  If you're heavily using resource-based
  # routes, you may want to comment/remove this line to prevent
  # clients from calling your create or destroy actions with a GET
  # r.default_routes
  
  # Change this for your home page to be available at /
  # r.match('/').to(:controller => 'whatever', :action =>'index')
end