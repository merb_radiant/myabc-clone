# Make the app's "gems" directory a place where gems are loaded from
Gem.clear_paths
Gem.path.unshift(Merb.root / "gems")


# Make the app's "lib" directory a place where ruby files get "require"d from
$LOAD_PATH.unshift(Merb.root / "lib")

### Uncomment for ActiveRecord ORM
use_orm :activerecord

unless defined? RADIANT_ROOT
  if File.directory?(root_path = "#{Merb.root}/vendor/radiant")
    RADIANT_ROOT = root_path
  else
    environment = IO.readlines("#{File.dirname(__FILE__)}/radiant_environment.rb").reject { |l| l.strip =~ /^#/ }.join
    environment =~ /[^#]\s*RADIANT_GEM_VERSION\s*=\s*(["'])([\d.]+)\1/
    version = $2

    require 'rubygems'
    if version and (radiant_gem = Gem.cache.search('radiant', version).first)
      if self.class.method_defined?(:gem)
        gem "radiant", "=#{version}"
        require "radiant"
      else
        require_gem "radiant", "=#{version}"
      end
    else
      STDERR.puts %(
Cannot find gem or source for Radiant #{version}:
  Install the missing gem with 'gem install -v=#{version} radiant', or change
  environment.rb to define RADIANT_GEM_VERSION with your desired version.
)
      exit 1
    end
  end
end

load File.join(RADIANT_ROOT, 'config', 'boot.rb')




Merb::Config.use do |c|
  c[:session_secret_key]  = '21df15c818c7fbf70c5c368379ddf2f2bba3518d'
  c[:session_store] = 'cookie'
end  

### Merb doesn't come with database support by default.  You need
### an ORM plugin.  Install one, and uncomment one of the following lines,
### if you need a database.

### Uncomment for DataMapper ORM
# use_orm :datamapper



### Uncomment for Sequel ORM
# use_orm :sequel


### This defines which test framework the generators will use
### rspec is turned on by default
# use_test :test_unit

#use_test :rspec

### Add your other dependencies here

# These are some examples of how you might specify dependencies.
# 
# dependencies "RedCloth", "merb_helpers"
# OR
# dependency "RedCloth", "> 3.0"
# OR
# dependencies "RedCloth" => "> 3.0", "ruby-aes-cext" => "= 1.0"

Merb::BootLoader.after_app_loads do
  ### Add dependencies here that must load after the application loads:

  # dependency "magic_admin" # this gem uses the app's model classes
end
