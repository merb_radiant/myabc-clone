desc "Set the environment variable Merb.environment='development'."
task :development do
  ENV['MERB_ENV'] = Merb.environment = 'development'
  Rake::Task[:environment].invoke
end

desc "Set the environment variable Merb.environment='production'."
task :production do
  ENV['MERB_ENV'] = Merb.environment = 'production'
  Rake::Task[:environment].invoke
end
