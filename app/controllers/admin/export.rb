module Admin
  class Export < Application
    def yaml
      render :text => Radiant::Exporter.export, :content_type => "text/yaml"
    end
  end
end